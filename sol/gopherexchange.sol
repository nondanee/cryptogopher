pragma solidity ^0.4.25;

import "./gopherfactory.sol";

contract GopherExchange is GopherFactory {

    event exchangeCast(uint activeId, uint passiveId);
    event exchangeCancel(uint activeId, uint passiveId);
    event exchangeFinish(uint activeId, uint passiveId);

    struct exchangeTransaction {
        uint activeId;
        uint passiveId;
    }

    exchangeTransaction[] public exchangeQueue; // exchange transaction id start from 1, 0 is preserved for none;

    function requestExchange(uint activeId, uint passiveId) external validGopher(activeId) validGopher(passiveId) returns (uint) {
        require(activeId != passiveId);
        require(gophers[activeId - 1].owner != gophers[passiveId - 1].owner);
        require(raiseByOwner(activeId));
        require(gophers[activeId - 1].exchange == 0 && gophers[activeId - 1].exchange == 0); // no pending both side
        require(withEqualLevel(activeId, passiveId)); // require level = level

        uint transactionId = exchangeQueue.push(exchangeTransaction(activeId, passiveId)); // don't minus 1
        gophers[activeId - 1].exchange = transactionId;
        gophers[passiveId - 1].exchange = transactionId;
        emit exchangeCast(activeId, passiveId);
        return transactionId;
    }

    function endExchange(exchangeTransaction storage transaction, bool done) internal {
        if (done)
            emit exchangeCancel(transaction.activeId, transaction.passiveId);
        else
            emit exchangeFinish(transaction.activeId, transaction.passiveId);
        gophers[transaction.activeId - 1].exchange = 0;
        gophers[transaction.passiveId - 1].exchange = 0;
        transaction.activeId = 0;
        transaction.passiveId = 0;
    }

    function withdrawRequest(uint transactionId) external validTransaction(transactionId) {
        exchangeTransaction storage transaction = exchangeQueue[transactionId - 1];
        require(raiseByOwner(transaction.activeId));
        endExchange(transaction, false);
    }

    function decideExchange(uint transactionId, bool agree) external validTransaction(transactionId) {
        exchangeTransaction storage transaction = exchangeQueue[transactionId - 1];
        require(raiseByOwner(transaction.passiveId));
        if (agree) exchangeGophers(transaction.activeId, transaction.passiveId);
        endExchange(transaction, agree);
    }

    modifier validTransaction(uint transactionId) {
        require(transactionId > 0);
        require(exchangeQueue[transactionId - 1].activeId > 0);
        require(exchangeQueue[transactionId - 1].passiveId > 0);
        _;
    }

}