pragma solidity ^0.4.25;

import "./gopherexchange.sol";

contract GopherFeeding is GopherExchange {

    uint32 interval = 1 minutes;
    mapping (uint32 => uint32) public readyTime;

    event FeedCast(uint32 accountId, uint gopherId, uint8 increment);

    function ready(uint32 accountId) internal view returns (bool) {
        return readyTime[accountId] <= now;
    }

    function randomIncrement(uint8 scale) internal constant returns (uint8) {
        return uint8(keccak256(abi.encodePacked(now, msg.sender))) % scale;
    }

    function feed(uint gopherId) external validGopher(gopherId) growingUp(gopherId) bound returns (uint8) {
        uint32 accountId = addressBinding[msg.sender];
        require(ready(accountId));
        Gopher storage gopher = gophers[gopherId - 1];

        uint8 increment = randomIncrement(6);
        if(gopher.owner == accountId)
            increment += 1; // 1 - 6
        else
            increment += 3; // 3 - 8

        gopher.level += increment;
        readyTime[accountId] = uint32(now + interval);

        if (gopher.exchange != 0) {
            exchangeTransaction storage transaction = exchangeQueue[gopher.exchange - 1];
            if (!withEqualLevel(transaction.activeId, transaction.passiveId)) {
                endExchange(transaction, false);
            }
        }
        emit FeedCast(accountId, gopherId, increment);
        return increment;
    }

}