const MetamaskInpageProvider = require('metamask-inpage-provider')
const PortStream = require('extension-port-stream')
const Web3 = require('web3')

const METAMASK_EXTENSION_ID = 'nkbihfbeogaeaoehlefnkodbefgpgknn'
const metamaskPort = chrome.runtime.connect(METAMASK_EXTENSION_ID)
const pluginStream = new PortStream(metamaskPort)
const provider = new MetamaskInpageProvider(pluginStream)

const web3 = new Web3(provider)
// web3.eth.getAccounts().then(e => console.log(e))
const contractAddress = '0xdbebcf444beb9269c9cca7af27f0d57c8ed9105c'
const contractABI = [{"constant":false,"inputs":[{"name":"activeId","type":"uint256"},{"name":"passiveId","type":"uint256"}],"name":"requestExchange","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint32"}],"name":"readyTime","outputs":[{"name":"","type":"uint32"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint32"},{"name":"","type":"uint256"}],"name":"ownership","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"accountId","type":"uint32"},{"name":"accountAddress","type":"address"}],"name":"bind","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"renounceOwnership","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"createGopher","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"transactionId","type":"uint256"}],"name":"withdrawRequest","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"gophers","outputs":[{"name":"DNA","type":"uint256"},{"name":"level","type":"uint8"},{"name":"owner","type":"uint32"},{"name":"exchange","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"isOwner","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"transactionId","type":"uint256"},{"name":"agree","type":"bool"}],"name":"decideExchange","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"checkBinding","outputs":[{"name":"","type":"uint32"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"gopherId","type":"uint256"}],"name":"feed","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"exchangeQueue","outputs":[{"name":"activeId","type":"uint256"},{"name":"passiveId","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"anonymous":false,"inputs":[{"indexed":false,"name":"accountId","type":"uint32"},{"indexed":false,"name":"gopherId","type":"uint256"},{"indexed":false,"name":"increment","type":"uint8"}],"name":"FeedCast","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"activeId","type":"uint256"},{"indexed":false,"name":"passiveId","type":"uint256"}],"name":"exchangeCast","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"activeId","type":"uint256"},{"indexed":false,"name":"passiveId","type":"uint256"}],"name":"exchangeCancel","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"activeId","type":"uint256"},{"indexed":false,"name":"passiveId","type":"uint256"}],"name":"exchangeFinish","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"previousOwner","type":"address"},{"indexed":true,"name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"}]
const contract = new web3.eth.Contract(contractABI, contractAddress)

// let feedEvent = contract.FeedCast()
// feedEvent.watch((error, result) => {
//     if (!error)
//         {
//             console.log(result)
//         } else {
//             console.log(error)
//         }
// })

// contract.events.FeedCast({
//     filter: {clusterAddress: [web3.eth.defaultAccount]},
//     fromBlock: 1899162,
//     toBlock: 'latest'})
//     .on('data', event => {
//         console.log('new event:', event)
//     })
//     .on('changed', event => {
//         console.log('event removed from blockchain:', event)
//     })
//     .on('error', error => {
//         console.error(error)
// })

// if (provider) {
//   console.log('provider detected', provider)
//   const eth = new ethjs(provider)
//   renderText('MetaMask provider detected.')
//   eth.accounts()
//   .then((accounts) => {
//     renderText(`Detected MetaMask account ${accounts[0]}`)
//   })
// }
// function renderText (text) {
//   content.innerText = text
// }

