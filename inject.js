(() => {

	const contractAddress = '0x153c2c52ffef7ce1b348d16628275c203c29dde8'
	const contractABI = [{"anonymous":false,"inputs":[{"indexed":true,"name":"previousOwner","type":"address"},{"indexed":true,"name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"constant":false,"inputs":[{"name":"accountId","type":"uint32"},{"name":"accountAddress","type":"address"}],"name":"bind","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"createGopher","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"transactionId","type":"uint256"},{"name":"agree","type":"bool"}],"name":"decideExchange","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"anonymous":false,"inputs":[{"indexed":false,"name":"activeId","type":"uint256"},{"indexed":false,"name":"passiveId","type":"uint256"}],"name":"exchangeCancel","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"activeId","type":"uint256"},{"indexed":false,"name":"passiveId","type":"uint256"}],"name":"exchangeFinish","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"activeId","type":"uint256"},{"indexed":false,"name":"passiveId","type":"uint256"}],"name":"exchangeCast","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"accountId","type":"uint32"},{"indexed":false,"name":"gopherId","type":"uint256"},{"indexed":false,"name":"increment","type":"uint8"}],"name":"FeedCast","type":"event"},{"constant":false,"inputs":[{"name":"gopherId","type":"uint256"}],"name":"feed","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"renounceOwnership","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"activeId","type":"uint256"},{"name":"passiveId","type":"uint256"}],"name":"requestExchange","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"transactionId","type":"uint256"}],"name":"withdrawRequest","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"checkBinding","outputs":[{"name":"","type":"uint32"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"exchangeQueue","outputs":[{"name":"activeId","type":"uint256"},{"name":"passiveId","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"gophers","outputs":[{"name":"DNA","type":"uint256"},{"name":"level","type":"uint8"},{"name":"owner","type":"uint32"},{"name":"exchange","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"isOwner","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint32"},{"name":"","type":"uint256"}],"name":"ownership","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint32"}],"name":"readyTime","outputs":[{"name":"","type":"uint32"}],"payable":false,"stateMutability":"view","type":"function"}]

	const artwork = [
		{
			"name": "Body",
			"images": [
				"https://user-images.githubusercontent.com/26399680/56563204-ab710780-65dd-11e9-81d2-cdea40fda07a.png",
				"https://user-images.githubusercontent.com/26399680/56563206-ab710780-65dd-11e9-837d-9dbeb05bd4a2.png",
				"https://user-images.githubusercontent.com/26399680/56563207-ab710780-65dd-11e9-92bb-18993babc5f9.png",
				"https://user-images.githubusercontent.com/26399680/56563208-ac099e00-65dd-11e9-9a88-0d2c9e8b1b67.png",
				"https://user-images.githubusercontent.com/26399680/56563209-ac099e00-65dd-11e9-89e9-10dfcc44815f.png",
				"https://user-images.githubusercontent.com/26399680/56563210-ac099e00-65dd-11e9-8c7f-8f77ae7e150c.png",
			]
		},
		{
			"name": "Eyes",
			"images": [
				"https://user-images.githubusercontent.com/26399680/56563296-e07d5a00-65dd-11e9-8b1d-f9fd9a6fadf1.png",
				"https://user-images.githubusercontent.com/26399680/56563298-e115f080-65dd-11e9-86ac-60b3754a5ddb.png",
				"https://user-images.githubusercontent.com/26399680/56563300-e115f080-65dd-11e9-8e00-9d2265fdb832.png",
				"https://user-images.githubusercontent.com/26399680/56563301-e115f080-65dd-11e9-8d46-c49fb9b21ab5.png",
				"https://user-images.githubusercontent.com/26399680/56563302-e1ae8700-65dd-11e9-9d27-9bdb839be3f3.png",
				"https://user-images.githubusercontent.com/26399680/56563304-e1ae8700-65dd-11e9-9a78-0f2f80ff1c0e.png",
				"https://user-images.githubusercontent.com/26399680/56563307-e2471d80-65dd-11e9-9f2f-83bb39efe9be.png",
				"https://user-images.githubusercontent.com/26399680/56563310-e2471d80-65dd-11e9-93f2-cd89c8aca41e.png",
				"https://user-images.githubusercontent.com/26399680/56563311-e2471d80-65dd-11e9-94c5-022b383df1fd.png",
			]
		},
		{
			"name": "Hair",
			"images": [
				"https://user-images.githubusercontent.com/26399680/56563062-74025b00-65dd-11e9-8e0d-508643cc76bd.png",
				"https://user-images.githubusercontent.com/26399680/56563063-74025b00-65dd-11e9-9f21-88f99e7a84b6.png",
				"https://user-images.githubusercontent.com/26399680/56563064-74025b00-65dd-11e9-9153-a9a7935d9816.png",
				"https://user-images.githubusercontent.com/26399680/56563066-74025b00-65dd-11e9-967c-d71f8e39d08d.png",
				"https://user-images.githubusercontent.com/26399680/56563068-749af180-65dd-11e9-89aa-1b4ae2a99033.png",
				"https://user-images.githubusercontent.com/26399680/56563069-749af180-65dd-11e9-8d3f-4073ccf41665.png",
				"https://user-images.githubusercontent.com/26399680/56563070-749af180-65dd-11e9-90db-3d9907a4ac48.png",
				"https://user-images.githubusercontent.com/26399680/56563071-75338800-65dd-11e9-8972-5e7f7ad380e3.png",
				"https://user-images.githubusercontent.com/26399680/56563072-75338800-65dd-11e9-9c73-9450ee189aa7.png",
				"https://user-images.githubusercontent.com/26399680/56563073-75338800-65dd-11e9-94be-280047c6bcef.png",
				"https://user-images.githubusercontent.com/26399680/56563075-75cc1e80-65dd-11e9-9fdd-ed496fa4ea2f.png",
				"https://user-images.githubusercontent.com/26399680/56563076-75cc1e80-65dd-11e9-86e0-81ec51da33d7.png",
				"https://user-images.githubusercontent.com/26399680/56563077-75cc1e80-65dd-11e9-9544-263042f7b306.png",
				"https://user-images.githubusercontent.com/26399680/56563078-7664b500-65dd-11e9-80b0-89dab5ed569f.png",
				"https://user-images.githubusercontent.com/26399680/56563079-7664b500-65dd-11e9-8098-8c45a1436cad.png",
				"https://user-images.githubusercontent.com/26399680/56563080-7664b500-65dd-11e9-81a5-68d8c650891f.png",
				"https://user-images.githubusercontent.com/26399680/56563083-7664b500-65dd-11e9-8d88-840b8e50f1d6.png",
				"https://user-images.githubusercontent.com/26399680/56563085-76fd4b80-65dd-11e9-98fc-c375c0e6a01d.png",
				"https://user-images.githubusercontent.com/26399680/56563086-76fd4b80-65dd-11e9-8841-31307e845e29.png",
				"https://user-images.githubusercontent.com/26399680/56563087-76fd4b80-65dd-11e9-9fbd-3db5392ee51a.png",
				"https://user-images.githubusercontent.com/26399680/56563088-7795e200-65dd-11e9-966e-768d3ce05b6d.png",
				"https://user-images.githubusercontent.com/26399680/56563089-7795e200-65dd-11e9-8839-76057acca37c.png",
				"https://user-images.githubusercontent.com/26399680/56563090-7795e200-65dd-11e9-8ed9-b40b1f5bb89f.png",
				"https://user-images.githubusercontent.com/26399680/56563092-782e7880-65dd-11e9-9e28-ccd7d69b1384.png",
				"https://user-images.githubusercontent.com/26399680/56563093-782e7880-65dd-11e9-96fb-43b1236ddbef.png",
				"https://user-images.githubusercontent.com/26399680/56563095-782e7880-65dd-11e9-97ce-8c0566317717.png",
				"https://user-images.githubusercontent.com/26399680/56563096-78c70f00-65dd-11e9-9f27-9a7d704f02ad.png",
				"https://user-images.githubusercontent.com/26399680/56563097-78c70f00-65dd-11e9-84a7-be54ae70e16d.png",
				"https://user-images.githubusercontent.com/26399680/56563098-78c70f00-65dd-11e9-8f50-1748a66fa487.png",
				"https://user-images.githubusercontent.com/26399680/56563099-78c70f00-65dd-11e9-9866-f2cc7a479be1.png",
				"https://user-images.githubusercontent.com/26399680/56563101-795fa580-65dd-11e9-8a46-6eee20320c79.png",
				"https://user-images.githubusercontent.com/26399680/56563103-795fa580-65dd-11e9-996a-0d889cf1a4d8.png",
				"https://user-images.githubusercontent.com/26399680/56563104-795fa580-65dd-11e9-88c8-dcb162c298ad.png",
				"https://user-images.githubusercontent.com/26399680/56563106-79f83c00-65dd-11e9-8bd9-2c2a81b61daf.png",
				"https://user-images.githubusercontent.com/26399680/56563107-79f83c00-65dd-11e9-93bc-cc40be5dba7c.png",
				"https://user-images.githubusercontent.com/26399680/56563109-79f83c00-65dd-11e9-9e6e-3110a4242735.png",
				"https://user-images.githubusercontent.com/26399680/56563110-7a90d280-65dd-11e9-949c-bff8e095d565.png",
				"https://user-images.githubusercontent.com/26399680/56563112-7a90d280-65dd-11e9-91c3-17667163b3f1.png",
				"https://user-images.githubusercontent.com/26399680/56563113-7a90d280-65dd-11e9-9e72-492ffb0ec851.png",
				"https://user-images.githubusercontent.com/26399680/56563114-7b296900-65dd-11e9-944c-0dad7a9f4021.png",
				"https://user-images.githubusercontent.com/26399680/56563115-7b296900-65dd-11e9-8694-5504d5f224f1.png",
				"https://user-images.githubusercontent.com/26399680/56563118-7cf32c80-65dd-11e9-9a85-f519d74107cf.png",
				"https://user-images.githubusercontent.com/26399680/56563126-7fee1d00-65dd-11e9-8238-415ccf4641bf.png",
				"https://user-images.githubusercontent.com/26399680/56563127-7fee1d00-65dd-11e9-8cad-b1d31f4174a1.png",
				"https://user-images.githubusercontent.com/26399680/56563128-8086b380-65dd-11e9-8d27-9d381807ae21.png",
				"https://user-images.githubusercontent.com/26399680/56563129-8086b380-65dd-11e9-8333-b40b3a732d6d.png",
				"https://user-images.githubusercontent.com/26399680/56563130-8086b380-65dd-11e9-82ba-70ddb08e645a.png",
				"https://user-images.githubusercontent.com/26399680/56563131-811f4a00-65dd-11e9-9f1d-c94b76537ac2.png",
				"https://user-images.githubusercontent.com/26399680/56563132-81b7e080-65dd-11e9-92ee-8266361f6835.png",
				"https://user-images.githubusercontent.com/26399680/56563135-82507700-65dd-11e9-9de6-fd573a6cd4a8.png",
				"https://user-images.githubusercontent.com/26399680/56563136-82507700-65dd-11e9-9d55-929cc48dd64c.png",
				"https://user-images.githubusercontent.com/26399680/56563137-82e90d80-65dd-11e9-9257-14c70a3b2996.png",
				"https://user-images.githubusercontent.com/26399680/56563138-82e90d80-65dd-11e9-8b1e-2be8959f251f.png",
			]
		},
		{
			"name": "Glasses",
			"images": [
				"https://user-images.githubusercontent.com/26399680/56562989-4cab8e00-65dd-11e9-8645-75825e2e4a4b.png",
				"https://user-images.githubusercontent.com/26399680/56562990-4d442480-65dd-11e9-9ccf-528be042dea5.png",
				"https://user-images.githubusercontent.com/26399680/56562991-4d442480-65dd-11e9-9b66-77dfc88085d2.png",
				"https://user-images.githubusercontent.com/26399680/56562993-4d442480-65dd-11e9-868d-483d52ec006f.png",
				"https://user-images.githubusercontent.com/26399680/56562994-4ddcbb00-65dd-11e9-8493-901260a19493.png",
				"https://user-images.githubusercontent.com/26399680/56562995-4ddcbb00-65dd-11e9-810b-eb710ed43d9d.png",
				"https://user-images.githubusercontent.com/26399680/56562997-4e755180-65dd-11e9-8f31-94605b70349b.png",
				"https://user-images.githubusercontent.com/26399680/56562998-4e755180-65dd-11e9-94b9-2fe23b4da7b0.png",
				"https://user-images.githubusercontent.com/26399680/56563000-4e755180-65dd-11e9-9cd3-366e8841e58d.png",
				"https://user-images.githubusercontent.com/26399680/56563001-4e755180-65dd-11e9-9fd2-e76a8790bd5e.png",
				"https://user-images.githubusercontent.com/26399680/56563003-4f0de800-65dd-11e9-9136-ca9bdd6facf1.png",
				"https://user-images.githubusercontent.com/26399680/56563004-4f0de800-65dd-11e9-89d4-96b31e2efc0b.png",
				"https://user-images.githubusercontent.com/26399680/56563005-4f0de800-65dd-11e9-85f6-f2e7f9043c1a.png",
				"https://user-images.githubusercontent.com/26399680/56563007-4fa67e80-65dd-11e9-853b-721f3b4e9708.png",
				"https://user-images.githubusercontent.com/26399680/56563009-4fa67e80-65dd-11e9-9832-511f755ae97f.png",
				"https://user-images.githubusercontent.com/26399680/56563010-4fa67e80-65dd-11e9-9d03-990d6435c7b9.png",
				"https://user-images.githubusercontent.com/26399680/56563012-503f1500-65dd-11e9-9add-9e067c1a08c8.png",
				"https://user-images.githubusercontent.com/26399680/56563013-503f1500-65dd-11e9-90d4-7688936fe96f.png",
				"https://user-images.githubusercontent.com/26399680/56563015-503f1500-65dd-11e9-819f-f071a9d036bd.png",
				"https://user-images.githubusercontent.com/26399680/56563016-503f1500-65dd-11e9-9545-45fa6ab3f5c8.png",
				"https://user-images.githubusercontent.com/26399680/56563017-50d7ab80-65dd-11e9-90b9-f43138bbac3c.png",
			]
		},
		{
			"name": "Extras",
			"images": [
				"https://user-images.githubusercontent.com/26399680/56562884-13731e00-65dd-11e9-85a2-206575f06691.png",
				"https://user-images.githubusercontent.com/26399680/56562887-13731e00-65dd-11e9-83b5-8872e43e0cec.png",
				"https://user-images.githubusercontent.com/26399680/56562883-12da8780-65dd-11e9-95a4-d2fbfaabedc9.png",
				"https://user-images.githubusercontent.com/26399680/56562895-14a44b00-65dd-11e9-965b-d4d7fd75c762.png",
				"https://user-images.githubusercontent.com/26399680/56562896-153ce180-65dd-11e9-8c37-fa34c8d8f114.png",
				"https://user-images.githubusercontent.com/26399680/56562888-140bb480-65dd-11e9-81b5-a72ff4389efe.png",
				"https://user-images.githubusercontent.com/26399680/56562885-13731e00-65dd-11e9-9614-e5ffe8ab9f07.png",
				"https://user-images.githubusercontent.com/26399680/56562889-140bb480-65dd-11e9-81a0-ae013ae944ad.png",
				"https://user-images.githubusercontent.com/26399680/56562890-140bb480-65dd-11e9-991b-440f980e8fda.png",
				"https://user-images.githubusercontent.com/26399680/56562894-14a44b00-65dd-11e9-909f-cf920cf183f6.png",
				"https://user-images.githubusercontent.com/26399680/56562891-14a44b00-65dd-11e9-9509-c1358f009580.png",
				"https://user-images.githubusercontent.com/26399680/56562892-14a44b00-65dd-11e9-8334-645f64bc0b8d.png",
				"https://user-images.githubusercontent.com/26399680/56562897-153ce180-65dd-11e9-9270-22d95f7d6d1d.png",
				"https://user-images.githubusercontent.com/26399680/56562898-153ce180-65dd-11e9-816c-fc03602904af.png",
			]
		}
	]

	const createElement = (tagName, className, innerHTML) => {
		let element = document.createElement(tagName)
		if(className) element.className = className
		if(innerHTML) element.innerHTML = innerHTML 
		return element
	}

	const call = (...arguments) => 
		new Promise((resolve, reject) => {
			arguments[0].apply(null, arguments.slice(1).concat([(error, result) => error ? reject(error) : resolve(result)]))
		})

	const gopherFigure = (DNA, level) => {
		let figure = createElement('div', 'gopher-figure')
		artwork.slice(0, artwork.length - 1).forEach(part => {
			let digits = part.images.length.toString().length * 2
			let perform = DNA.slice(0, digits)
			DNA = DNA.slice(digits)
			figure.appendChild(createElement('div', 'gopher-layer')).style.backgroundImage = `url(${part.images[parseInt(perform) % part.images.length]})`
		})
		figure.appendChild(createElement('div', 'gopher-layer')).style.backgroundImage = `url(${artwork[artwork.length - 1].images[level]})`
		return figure
	}

	const lookupTransaction = id => 
		call(contract.exchangeQueue, id - 1)
		.then(data => data.map(r => r.c.join('')))
			
	const drawGopher = item => {
		
		let container = createElement('div', 'gopher-container')
		container.title = `DNA: ${item.DNA}\nlevel: ${item.level}\nexchange: ${item.exchange}\n`
		let figure = gopherFigure(item.DNA, item.level)
		let operation = createElement('div', 'gopher-operation')
		container.appendChild(figure)
		container.appendChild(operation)
		operation.appendChild(createElement('button', 'gopher-operation-feed', 'feed')).onclick = () => {
			call(contract.feed, item.id).then(e => console.log(e))
		}
		
		if(selfId != userId && item.exchange == 0){
			if(ableExchange(item.level)){
				operation.appendChild(createElement('button', 'gopher-operation-exchange', 'exchange')).onclick = () => {
					// choose id
					call(contract.requestExchange, selfGopher[0].id, item.id).then(e => console.log(e))
				}
			}
		}
		if(selfId == userId && item.exchange != 0){
			return lookupTransaction(item.exchange).then(transaction => {
				if(transaction[0] == item.id){
					operation.appendChild(createElement('button', 'gopher-exchange-agree', 'withdraw')).onclick = () => {
						call(contract.withdrawRequest, item.exchange).then(e => console.log(e))
					}
				}
				else{
					operation.appendChild(createElement('button', 'gopher-exchange-agree', 'agree')).onclick = () => {
						call(contract.decideExchange, item.exchange, true).then(e => console.log(e))
					}
					operation.appendChild(createElement('button', 'gopher-exchange-disagree', 'disagree')).onclick = () => {
						call(contract.decideExchange, item.exchange, false).then(e => console.log(e))
					}
				}
				return container
			})			
		}
		return container
	}

	const ableExchange = level => {
		return selfGopher.some(gopher => gopher.level == level)
	}

	const ableCreate = gophers => {
		return gophers.length == 0 || gophers.some(gopher => gopher.level == 13)
	}

	const fillGallery = (gallery, gophers) => {
		gallery.innerHTML = ''
		return Promise.all(gophers.map(drawGopher)).then(elements => elements.forEach(element => gallery.appendChild(element)))
	}

	const getGopherById = id => 
		call(contract.gophers, id - 1).then(data => ({
			id: id,
			DNA: data[0].c.join(''),
			level: parseInt(data[1].c / 10),
			owner: data[2].c.join(''),
			exchange: data[3].c.join('')
		}))

	const getGopherByOwner = owner => 
		call(contract.ownership, owner, null).then(data => data.c)
		.then(ids => Promise.all(ids.filter(id => id > 0).map(getGopherById)))
	
	let contract = {}
	let userId = 0;
	let selfId = 0;
	let userGopher = []
	let selfGopher = []

	const init = () => {
		let target = document.getElementsByClassName('h-card')[0]
		if (!target) return

		let connector = new Web3(web3.currentProvider)
		ethereum.enable()

		contract = connector.eth.contract(contractABI).at(contractAddress)
		
		let avatar = target.getElementsByTagName('img')[0]
		userId = avatar.src.match(/\/u\/(\d+)/)[1]
		let gallery = target.appendChild(createElement('div', 'gopher-gallery'))

		call(contract.checkBinding).then(data => data.c.join(''))
		.then(id => selfId = id)
		.then(() => Promise.all([
			getGopherByOwner(userId).then(gophers => {
				userGopher = gophers
				if (selfId == userId) selfGopher = gophers
			}),
			selfId != userId ? getGopherByOwner(selfId).then(gophers => {
				selfGopher = gophers
			}) : null
		]))
		.then(() => fillGallery(gallery, userGopher))
		.then(() => {
			if(selfId == 0 &&!!target.getElementsByClassName('js-profile-editable-edit-button')){
				target.appendChild(createElement('button', 'gopher-register', 'Get start')).onclick = () => 
					register()				
			}
			if(selfId == userId && ableCreate(selfGopher)){
				target.appendChild(createElement('button', 'gopher-create', 'Create Gopher')).onclick = () => 
					call(contract.createGopher).then(() => {
						getGopherByOwner(userId).then(gophers => {
							userGopher = gophers
							selfGopher = gophers
							fillGallery(gallery, gophers)
						})
					})
			}
		})

	}

	const register = () => {
		const clientId = '9d436a9adeb17928e821'
		const redirectUri = `https://cryptogopher.herokuapp.com/oauth/callback/bind?address=${web3.eth.defaultAccount}`
		window.open(`https://github.com/login/oauth/authorize?client_id=${clientId}&redirect_uri=${redirectUri}`)
	}

	init()

})()
