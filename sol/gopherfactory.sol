pragma solidity ^0.4.25;

import "./accounthelper.sol";

contract GopherFactory is AccountHelper {

    struct Gopher {
        uint DNA;
        uint8 level;
        uint32 owner;
        uint exchange; // exchange transaction id start from 1, 0 is preserved for none;
    }

    uint8 maxLevel = 130;
    Gopher[] public gophers;
    mapping (uint32 => uint[]) public ownership; // gopher id start from 1, 0 is preserved for deleted;

    function generateDNA() private view returns (uint) {
        return uint(keccak256(abi.encodePacked(now, msg.sender, gophers.length))) % (10 ** 16);
    }

    function createGopher() external bound returns (uint) {
        uint32 accountId = addressBinding[msg.sender];
        require(withGenerationOpportunity(accountId));
        uint DNA = generateDNA();
        uint gopherId = gophers.push(Gopher(DNA, 0, accountId, 0)); // don't minus 1
        ownership[accountId].push(gopherId);
        return gopherId;
    }

    function withEqualLevel(uint gopherIdOne, uint gopherIdTwo) internal view returns (bool) {
        uint8 levelOne = gophers[gopherIdOne - 1].level / 10;
        uint8 levelTwo = gophers[gopherIdTwo - 1].level / 10;
        return levelOne == levelTwo;
    }

    function withGenerationOpportunity(uint32 accountId) internal view returns (bool) {
        bool result = true;
        uint[] memory owns = ownership[accountId];
        for (uint index = 0; index < owns.length; index++) {
            if (owns[index] != 0) {
                if (gophers[owns[index] - 1].level < maxLevel) {
                    result = false;
                    break;
                }
            }
        }
        return result;
    }

    function exchangeGophers(uint gopherIdOne, uint gopherIdTwo) internal {
        uint32 accountIdOne = gophers[gopherIdOne - 1].owner;
        uint32 accountIdTwo = gophers[gopherIdTwo - 1].owner;
        exchangeOwnership(accountIdOne, gopherIdOne, gopherIdTwo);
        exchangeOwnership(accountIdTwo, gopherIdTwo, gopherIdOne);
        gophers[gopherIdOne - 1].owner = accountIdTwo;
        gophers[gopherIdTwo - 1].owner = accountIdOne;
    }

    function exchangeOwnership(uint32 accountId, uint gopherIdFrom, uint gopherIdTo) internal {
        uint[] storage owns = ownership[accountId];
        for (uint index = 0; index < owns.length; index++) {
            if (owns[index] == gopherIdFrom) {
                owns[index] = gopherIdTo;
                break;
            }
        }
    }

    function raiseByOwner(uint gopherId) internal view returns (bool) {
        return gophers[gopherId - 1].owner == addressBinding[msg.sender];
    }

    modifier validGopher(uint gopherId) {
        require(gopherId > 0);
        _;
    }

    modifier growingUp(uint gopherId) {
        require(gophers[gopherId - 1].level < maxLevel);
        _;
    }

}