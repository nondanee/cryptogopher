// https://stackoverflow.com/questions/9263671/google-chrome-application-shortcut-how-to-auto-load-javascript/9310273#9310273
// https://stackoverflow.com/questions/9515704/insert-code-into-the-page-context-using-a-content-script/9517879#9517879

(() => {
	let dependency = (document.head || document.documentElement).appendChild(document.createElement('script'))
	dependency.src = chrome.extension.getURL('web3.min.js') // 'https://cdn.jsdelivr.net/gh/ethereum/web3.js/dist/web3.min.js'
	dependency.onload = () => dependency.parentNode.removeChild(dependency)

	let runner = (document.head || document.documentElement).appendChild(document.createElement('script'))
	runner.src = chrome.extension.getURL('inject.js')
	runner.onload = () => runner.parentNode.removeChild(runner)

	let prettifier = (document.head || document.documentElement).appendChild(document.createElement('link'))
	prettifier.rel = 'stylesheet', prettifier.type = 'text/css'
	prettifier.href = chrome.extension.getURL('inject.css')
})()