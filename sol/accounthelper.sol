pragma solidity ^0.4.25;

import "./ownable.sol";

contract AccountHelper is Ownable {

    // mapping (uint32 => address) internal idBinding;
    mapping (address => uint32) internal addressBinding;

    function bind(uint32 accountId, address accountAddress) external onlyOwner {
        addressBinding[accountAddress] = accountId;
    }

    function checkBinding() external view returns (uint32) {
        return addressBinding[msg.sender];
    }

    modifier bound() {
        require(addressBinding[msg.sender] != 0);
        _;
    }

}